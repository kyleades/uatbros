﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPointManager : MonoBehaviour
{

    public static CheckPointManager instance;

    public int checkpoint = -1;
    public List<GameObject> checkPoints;

    private void Awake()
    {
        // Setup the singleton
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void SaveCheckPoint()
    {
        PlayerPrefs.SetInt("CheckPoint", checkpoint);
    }

    public void LoadCheckPoint()
    {
        checkpoint = PlayerPrefs.GetInt("CheckPoint", 0);
        GameManager.instance.player.PlaceAtPosition(checkPoints[checkpoint].transform.position);
    }
}