﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : MonoBehaviour {

    public GameObject mCamera;
    public int playerSpeed =10;
    public int playerJumpPower = 1250;
    private float moveX;
    public bool isGrounded;
    public float distanceToBottomPlayer = 1.3f;
    public float distanceToTopPlayer = 1.3f;
    public AudioClip jumpClip;

    private int extraJumpValue;
    public int extraJumps;


    // Use this for initialization
    void Start () {
        CheckPointManager.instance.SaveCheckPoint();
    }
	
	// Update is called once per frame
	void Update () {
        PlayerMove();
        PlayerRaycast();
        if(isGrounded == true)
        {
            extraJumpValue = extraJumps;
        }
        

	}

    void PlayerMove()
    {
        //control
        moveX = Input.GetAxis("Horizontal");

        if (Input.GetButtonDown ("Jump") && extraJumpValue > 0)
        {
            Jump();
            GetComponent<Animator>().SetBool("IsJumping", true);
        }
        
        if (moveX != 0)
        {
            GetComponent<Animator>().SetBool("IsRunning", true);
        }
        else
        {
            GetComponent<Animator>().SetBool("IsRunning", false);
        }

        if (moveX < 0.0f)
        {
            GetComponent<SpriteRenderer>().flipX = true;
        }
        else if (moveX > 0.0f )
        {
            GetComponent<SpriteRenderer>().flipX = false;
        }

        gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(moveX * playerSpeed, gameObject.GetComponent<Rigidbody2D>().velocity.y);

    }

    // jumping
    void Jump()
    {
        GetComponent<Rigidbody2D>().AddForce(Vector2.up * playerJumpPower);
        isGrounded = false;
        AudioSource.PlayClipAtPoint(jumpClip, transform.position);
        extraJumpValue--;

    }

    public void PlaceAtPosition(Vector3 pos)
    {
        transform.position = pos;
        MoveCamera();
    }

    void MoveCamera()
    {
        mCamera.transform.position = new Vector3(transform.position.x, mCamera.transform.position.y, mCamera.transform.position.z);
    }


   
    void PlayerRaycast()
    {
        RaycastHit2D rayUp = Physics2D.Raycast(transform.position, Vector2.up);
        if ( rayUp.collider != null && rayUp.distance < distanceToTopPlayer && rayUp.collider.name == "LootBox")
        {
            Destroy(rayUp.collider.gameObject);
        }

        RaycastHit2D rayDown = Physics2D.Raycast(transform.position, Vector2.down);
        if (rayDown != null && rayDown.collider != null && rayDown.distance < distanceToBottomPlayer && rayDown.collider.tag == "enemy")
        {
            GetComponent<Rigidbody2D>().AddForce(Vector2.up * 1000);
            rayDown.collider.gameObject.GetComponent<Rigidbody2D>().AddForce(Vector2.right * 200);
            rayDown.collider.gameObject.GetComponent<Rigidbody2D>().gravityScale = 8;
            rayDown.collider.gameObject.GetComponent<Rigidbody2D>().freezeRotation = false;
            rayDown.collider.gameObject.GetComponent<BoxCollider2D>().enabled = false;
            rayDown.collider.gameObject.GetComponent<EnemyMove>().enabled = false;
            //Destroy(hit.collider.gameObject);
        }
        if (rayDown != null && rayDown.collider != null && rayDown.distance < distanceToBottomPlayer && rayDown.collider.tag != "enemy")
        {
            isGrounded = true;
            GetComponent<Animator>().SetBool("IsJumping", false);

        }
    }
    
}
